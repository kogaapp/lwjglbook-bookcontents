# 最初のステップ

この本では、3Dゲームの開発に関わる主要な技術について学びます。サンプルをJavaで開発し、Lightweight Java Game Library \([LWJGL](http://www.lwjgl.org/)\) を使用します。 LWJGLライブラリを使用すると、OpenGLなどの低レベルAPI \(Application Programming Interface\) にアクセスできます。

LWJGLは、OpenGLのラッパーのように動作する低レベルのAPIです。短時間で3Dゲームを作成することを考えているのであれば、 \[JmonkeyEngine\] のような他の選択肢を検討する必要があります。この低レベルのAPIを使用するには、多くの概念を理解し、結果を見る前にたくさんのコード行を書く必要があります。これを使用する利点は、3Dグラフィックスの理解を深め、より細かくコントロールをすることができることです。

前の段落で述べたように、この本ではJavaを使用します。 Java 9を使用するため、OracleのページからJava SDKをダウンロードする必要があります。あなたのオペレーティングシステムに合ったインストーラを選んでインストールしてください。この本は、あなたがJava言語を適度に理解していることを前提としています。

サンプルを実行するために必要なJava IDEを使用します。 Java 9をサポートしているIntelliJ IDEAをダウンロードします。Java 9は64ビットプラットフォームでのみ利用可能ですので、IntelliJの64ビットバージョンをダウンロードしてください。IntelliJは無料のオープンソースバージョンであるコミュニティ版を提供しています。次のリンクからダウンロードできます。 [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/ "Intellij")

![](/chapter01/intellij.png)

サンプルを作成するには、 [Maven](https://maven.apache.org/) を使用します。MavenはすでにほとんどのIDEで使用できるため、サンプルを直接開くことができます。サンプルを含むフォルダを開くだけで、IntelliJがMavenプロジェクトであることを検出します。

![](/chapter01/maven_project.png)

Mavenは、プロジェクト依存関係 \(使用する必要があるライブラリ\) とビルドプロセス中に実行されるステップを管理する `pom.xml`  \(Project Object Model\) という名前のXMLファイルに基づいてプロジェクトをビルドします。Mavenは設定上の規則の原則に従います。つまり、標準のプロジェクト構造と命名規則に従えば、設定ファイルは、ソースファイルがどこにあるか、コンパイルされたクラスがどこに置かれるべきかを明示的に言う必要はありません。

この本はMavenチュートリアルではありませんので、必要に応じてWeb上の情報を参考にしてください。 ソースコードフォルダは、使用するプラグインを定義し、使用するライブラリのバージョンを収集する親プロジェクトを定義します。

LWJGL 3.1では、プロジェクトの構築方法にいくつかの変更が加えられました。今ではベースコードはモジュール化されているので、巨大なモノリシックなjarファイルを使う代わりに、使いたいパッケージを選択して使うことができます。時間が掛かるものの依存関係を慎重に決める必要があります。しかし、 [ダウンロード](https://www.lwjgl.org/download)  ページでpomファイルを簡単に作成することができます。ここでは、GLFW bindingsとOpenGL bindingsのみを使用します。pomファイルがソースコード内でどのようになるかを確認することができます。

LWJGLプラットフォームの依存関係は、既にプラットフォーム用のネイティブライブラリを含んでいるので、他のプラグインを使用する必要はありません \(例えば`mavennatives`など\)  。LWJGLでプラットフォームを構成するプロパティーを設定するためには、3つのプロファイルを設定するだけです。Windows、Linux、およびMac OSのいずれかの値をプロパティに設定します。

```xml
    <profiles>
        <profile>
            <id>windows-profile</id>
            <activation>
                <os>
                    <family>Windows</family>
                </os>
            </activation>
            <properties>
                <native.target>natives-windows</native.target>
            </properties>                
        </profile>
        <profile>
            <id>linux-profile</id>
            <activation>
                <os>
                    <family>Linux</family>
                </os>
            </activation>
            <properties>
                <native.target>natives-linux</native.target>
            </properties>                
        </profile>
        <profile>
            <id>OSX-profile</id>
            <activation>
                <os>
                    <family>mac</family>
                </os>
            </activation>
            <properties>
                <native.target>natives-osx</native.target>
            </properties>
        </profile>
    </profiles>
```

各プロジェクトの中で、LWJGLのプラットフォームの依存関係は、開発環境のプラットフォームの設定されているプロパティーを使用します。

```xml
        <dependency>
            <groupId>org.lwjgl</groupId>
            <artifactId>lwjgl-platform</artifactId>
            <version>${lwjgl.version}</version>
            <classifier>${native.target}</classifier>
        </dependency>
```

それに加えて、すべてのプロジェクトは実行可能なjarファイルを生成します \(`java -jar name_of_the_jar.jar` とコマンドを入力することで実行することができます\)。maven-jar-pluginを使用して`MANIFEST.MF`ファイルを持つjarファイルを作成することができます。そのファイルの中で最も重要な属性は`Main-Class`です。これはプログラムのエントリーポイントを設定します。さらに、すべての依存関係は、そのファイルの`Class-Path`属性のエントリとして設定されます。それを別のコンピュータで実行するには、ターゲットディレクトリの下にあるメインjarファイルとlibディレクトリ \(中にあるjarファイルとともに\) をコピーするだけです。

LWJGLクラスを含むjarには、ネイティブライブラリも含まれています。 LWJGLはそれらを抽出し、ライブラリをJVMが見つけるためのパスを追加します。

第1章のソースコードは、LWJGLサイトのGET STARTEDのサンプルを使います  \([http://www.lwjgl.org/guide](http://www.lwjgl.org/guide)\)  。GUIライブラリとしてSwingやJavaFXを使用していないことがわかります。その代わりに、我々は、GUIコンポーネント  \(ウィンドウなど\) とイベント \(キーを押したときやマウスの移動など\) を扱うためのライブラリである [GLFW](www.glfw.org) を使用しています。[GLFW](www.glfw.org)で、簡単にOpenGLコンテキストを使用することができます。以前のバージョンのLWJGLではカスタムGUIのAPIが提供されていましたが、LWJGL 3ではGLFWが最も適切なAPIです。

サンプルのソースコードは非常によく文書化されており、ここでコメントを繰り返しています。

あなたの環境が正しく設定されていれば、実行すると赤い背景のウィンドウを見ることができるはずです。

![Hello World](hello_world.png)

**これらのソースコードは** [**GitHub**](https://github.com/lwjglgamedev/lwjglbook) **で公開されています。**

